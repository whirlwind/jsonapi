//
//  SBJsonParser.h
//  food
//
//  Created by 詹 迟晶 on 12-5-3.
//  Copyright (c) 2012年 BOOHEE. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SBJsonParser : NSObject

/**
 @brief Return the object represented by the given string
 
 Returns the object represented by the passed-in string or nil on error. The returned object can be
 a string, number, boolean, null, array or dictionary.
 
 @param repr the json string to parse
 */
- (id)objectWithString:(NSString *)repr;

/**
 @brief Return the object represented by the given string
 
 Returns the object represented by the passed-in string or nil on error. The returned object can be
 a string, number, boolean, null, array or dictionary.
 
 @param jsonText the json string to parse
 @param error pointer to an NSError object to populate on error
 */

- (id)objectWithString:(NSString*)jsonText
                 error:(NSError**)error;

@end
