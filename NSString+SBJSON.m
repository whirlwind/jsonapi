//
//  JsonKit+SBJson.m
//  food
//
//  Created by 詹 迟晶 on 12-5-3.
//  Copyright (c) 2012年 BOOHEE. All rights reserved.
//

#import "NSString+SBJSON.h"
#import "NSJSONSerialization+JSONKit.h"

@implementation NSString (SBJson)

- (id)JSONValue{
    return [self universalConvertToJSONObject];
}
@end
