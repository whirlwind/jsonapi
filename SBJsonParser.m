//
//  SBJsonParser.m
//  food
//
//  Created by 詹 迟晶 on 12-5-3.
//  Copyright (c) 2012年 BOOHEE. All rights reserved.
//

#import "SBJsonParser.h"
#import "NSJSONSerialization+JSONKit.h"

@implementation SBJsonParser

- (id)objectWithString:(NSString *)repr{
    return [repr universalConvertToJSONObject];
}
- (id)objectWithString:(NSString*)jsonText
                 error:(NSError**)error{
    return [self objectWithString:jsonText];
}
@end
