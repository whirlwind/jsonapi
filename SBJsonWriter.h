//
//  SBJsonWriter.h
//  iNice
//
//  Created by Whirlwind on 12-10-8.
//  Copyright (c) 2012年 BOOHEE. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SBJsonWriter : NSObject

/**
 @brief Return JSON representation (or fragment) for the given object.

 Returns a string containing JSON representation of the passed in value, or nil on error.
 If nil is returned and @p error is not NULL, @p *error can be interrogated to find the cause of the error.

 @param value any instance that can be represented as a JSON fragment

 */
- (NSString *)stringWithObject:(id)value;

@end
