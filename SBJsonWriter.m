//
//  SBJsonWriter.m
//  iNice
//
//  Created by Whirlwind on 12-10-8.
//  Copyright (c) 2012年 BOOHEE. All rights reserved.
//

#import "SBJsonWriter.h"
#import "NSJSONSerialization+JSONKit.h"

@implementation SBJsonWriter
- (NSString *)stringWithObject:(id)value {
    [value retain];
    NSString *ret = nil;
    if ([value isKindOfClass:[NSString class]]) {
        ret = [(NSString *)value universalConvertToJSONString];
    }else if ([value isKindOfClass:[NSArray class]]) {
        ret = [(NSArray *)value universalConvertToJSONString];
    }else if ([value isKindOfClass:[NSDictionary class]]) {
        ret = [(NSDictionary *)value universalConvertToJSONString];
    }
    [value release];
    return ret;
}
@end
