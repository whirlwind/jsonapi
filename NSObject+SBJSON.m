//
//  NSObject+SBJSON.m
//  food
//
//  Created by 詹 迟晶 on 12-5-3.
//  Copyright (c) 2012年 BOOHEE. All rights reserved.
//

#import "NSObject+SBJSON.h"
#import "NSJSONSerialization+JSONKit.h"

@implementation NSObject (SBJSON)
- (NSString *)JSONRepresentation {
    if ([self class] == [NSString class]) {
        return [(NSString *)self universalConvertToJSONString];
    }else if ([self class] == [NSArray class]) {
        return [(NSArray *)self universalConvertToJSONString];
    }else if ([self class] == [NSDictionary class]) {
        return [(NSDictionary *)self universalConvertToJSONString];
    }
    return nil;
}
@end
