//
//  NSJSONSerialization+JSONKit.m
//  food
//
//  Created by 詹 迟晶 on 12-2-19.
//  Copyright (c) 2012年 BOOHEE. All rights reserved.
//

#import "NSJSONSerialization+JSONKit.h"

#ifndef OnlyUseJSONKit
    #define OnlyUseJSONKit 0
#endif

#if __IPHONE_5_0 > __IPHONE_OS_VERSION_MIN_REQUIRED || OnlyUseJSONKit == 1
#import "JSONKit.h"
#endif

#ifdef __OPTIMIZE__
#   define ModuleLog(...) /**/
#else
#   ifdef DEBUG_JSON
#       define ModuleLog(...) NSLog(__VA_ARGS__)
#   else
#       define ModuleLog(...) /**/
#   endif
#endif

#pragma mark - Deserializing
@implementation NSString (JSONDeserializing)
- (id)universalConvertToJSONObject{
#if OnlyUseJSONKit == 0
#if __IPHONE_5_0 > __IPHONE_OS_VERSION_MIN_REQUIRED
    if ([[[UIDevice currentDevice] systemVersion] intValue] >= 5) {
#endif
        ModuleLog(@"Using iOS5's NSJSONSerialization to deserialize JSON.");
        NSData *data = [self dataUsingEncoding:NSUTF8StringEncoding];
        NSError *error = nil;
        id jsonObj = [NSJSONSerialization JSONObjectWithData:data
                                                     options:NSJSONReadingMutableContainers|NSJSONReadingMutableLeaves
                                                       error:&error];
#ifndef __OPTIMIZE__
        if (error) {
            ModuleLog(@"JSON to Objcet error: %@", error);
        }
#endif
        return jsonObj;
#if __IPHONE_5_0 > __IPHONE_OS_VERSION_MIN_REQUIRED
    }else{
        ModuleLog(@"Using JSONKit to deserialize JSON.");
        return [self objectFromJSONString];
    }
#endif
#else
    ModuleLog(@"Using JSONKit to deserialize JSON.");
    return [self objectFromJSONString];
#endif
}

- (id)universalConvertToJSONMutableObject{
#if OnlyUseJSONKit == 0
#if __IPHONE_5_0 > __IPHONE_OS_VERSION_MIN_REQUIRED
    if ([[[UIDevice currentDevice] systemVersion] intValue] >= 5) {
#endif
        return [self universalConvertToJSONObject];
#if __IPHONE_5_0 > __IPHONE_OS_VERSION_MIN_REQUIRED
    }else{
        ModuleLog(@"Using JSONKit to deserialize JSON.");
        return [self mutableObjectFromJSONString];
    }
#endif
#else
    ModuleLog(@"Using JSONKit to deserialize JSON.");
    return [self mutableObjectFromJSONString];
#endif
}
@end

@implementation NSData (JSONDeserializing)
- (id)universalConvertToJSONObject{
#if OnlyUseJSONKit == 0
#if __IPHONE_5_0 > __IPHONE_OS_VERSION_MIN_REQUIRED
    if ([[[UIDevice currentDevice] systemVersion] intValue] >= 5) {
#endif
        ModuleLog(@"Using iOS5's NSJSONSerialization to deserialize JSON.");
        NSError *error = nil;
        id jsonObj = [NSJSONSerialization JSONObjectWithData:self
                                                     options:NSJSONReadingMutableContainers|NSJSONReadingMutableLeaves
                                                       error:&error];
#ifndef __OPTIMIZE__
        if (error) {
            ModuleLog(@"JSON to Objcet error: %@", error);
        }
#endif
        return jsonObj;
#if __IPHONE_5_0 > __IPHONE_OS_VERSION_MIN_REQUIRED
    }else{
        ModuleLog(@"Using JSONKit to deserialize JSON.");
        return [self objectFromJSONData];
    }
#endif
#else
    ModuleLog(@"Using JSONKit to deserialize JSON.");
    return [self objectFromJSONData];
#endif
}

- (id)universalConvertToJSONMutableObject{
#if __IPHONE_5_0 > __IPHONE_OS_VERSION_MIN_REQUIRED
    if ([[[UIDevice currentDevice] systemVersion] intValue] >= 5) {
#endif
        return [self universalConvertToJSONObject];
#if __IPHONE_5_0 > __IPHONE_OS_VERSION_MIN_REQUIRED
    }else{
        ModuleLog(@"Using JSONKit to deserialize JSON.");
        return [self mutableObjectFromJSONData];
    }
#endif
}
@end

#pragma mark Serializing methods
@implementation NSString (JSONSerializing)
- (NSData *)universalConvertToJSONData{
#if OnlyUseJSONKit == 0
#if __IPHONE_5_0 > __IPHONE_OS_VERSION_MIN_REQUIRED
    if ([[[UIDevice currentDevice] systemVersion] intValue] >= 5) {
#endif
        ModuleLog(@"Using iOS5's NSJSONSerialization to serialize JSON.");
        NSError *error = nil;
        NSData *data = [NSJSONSerialization dataWithJSONObject:self
                                        options:NSJSONWritingPrettyPrinted
                                                         error:&error];
#ifndef __OPTIMIZE__
        if (error) {
            ModuleLog(@"NSString to JSONData error: %@", error);
        }
#endif
        return data;
#if __IPHONE_5_0 > __IPHONE_OS_VERSION_MIN_REQUIRED
    }else{
        ModuleLog(@"Using JSONKit to serialize JSON.");
        return [self JSONData];
    }
#endif
#else
    ModuleLog(@"Using JSONKit to serialize JSON.");
    return [self JSONData];
#endif
}

- (NSString *)universalConvertToJSONString{
#if OnlyUseJSONKit == 0
#if __IPHONE_5_0 > __IPHONE_OS_VERSION_MIN_REQUIRED
    if ([[[UIDevice currentDevice] systemVersion] intValue] >= 5) {
#endif
        ModuleLog(@"Using iOS5's NSJSONSerialization to serialize JSON.");
        NSError *error = nil;
        NSData *data = [NSJSONSerialization dataWithJSONObject:self
                                                       options:NSJSONWritingPrettyPrinted
                                                         error:&error];
        return [[[NSString alloc] initWithData:data
                                      encoding:NSUTF8StringEncoding] autorelease];
#if __IPHONE_5_0 > __IPHONE_OS_VERSION_MIN_REQUIRED
    }else{
        ModuleLog(@"Using JSONKit to serialize JSON.");
        return [self JSONString];
    }
#endif
#else
    ModuleLog(@"Using JSONKit to serialize JSON.");
    return [self JSONString];
#endif
}
@end

@implementation NSArray (JSONSerializing)
- (NSData *)universalConvertToJSONData{
#if OnlyUseJSONKit == 0
#if __IPHONE_5_0 > __IPHONE_OS_VERSION_MIN_REQUIRED
    if ([[[UIDevice currentDevice] systemVersion] intValue] >= 5) {
#endif
        ModuleLog(@"Using iOS5's NSJSONSerialization to serialize JSON.");
        NSError *error = nil;
        NSData *data = [NSJSONSerialization dataWithJSONObject:self
                                                       options:NSJSONWritingPrettyPrinted
                                                         error:&error];
#ifndef __OPTIMIZE__
        if (error) {
            ModuleLog(@"NSArray to JSONData error: %@", error);
        }
#endif
        return data;
#if __IPHONE_5_0 > __IPHONE_OS_VERSION_MIN_REQUIRED
    }else{
        ModuleLog(@"Using JSONKit to serialize JSON.");
        return [self JSONData];
    }
#endif
#else
    ModuleLog(@"Using JSONKit to serialize JSON.");
    return [self JSONData];
#endif
}

- (NSString *)universalConvertToJSONString{
#if OnlyUseJSONKit == 0
#if __IPHONE_5_0 > __IPHONE_OS_VERSION_MIN_REQUIRED
    if ([[[UIDevice currentDevice] systemVersion] intValue] >= 5) {
#endif
        ModuleLog(@"Using iOS5's NSJSONSerialization to serialize JSON.");
        NSError *error = nil;
        NSData *data = [NSJSONSerialization dataWithJSONObject:self
                                                       options:NSJSONWritingPrettyPrinted
                                                         error:&error];
        return [[[NSString alloc] initWithData:data
                                      encoding:NSUTF8StringEncoding] autorelease];
#if __IPHONE_5_0 > __IPHONE_OS_VERSION_MIN_REQUIRED
    }else{
        ModuleLog(@"Using JSONKit to serialize JSON.");
        return [self JSONString];
    }
#endif
#else
    ModuleLog(@"Using JSONKit to serialize JSON.");
    return [self JSONString];
#endif
}
@end

@implementation NSDictionary (JSONSerializing)
- (NSData *)universalConvertToJSONData{
#if OnlyUseJSONKit == 0
#if __IPHONE_5_0 > __IPHONE_OS_VERSION_MIN_REQUIRED
    if ([[[UIDevice currentDevice] systemVersion] intValue] >= 5) {
#endif
        ModuleLog(@"Using iOS5's NSJSONSerialization to serialize JSON.");
        NSError *error = nil;
        NSData *data = [NSJSONSerialization dataWithJSONObject:self
                                                       options:NSJSONWritingPrettyPrinted
                                                         error:&error];
#ifndef __OPTIMIZE__
        if (error) {
            ModuleLog(@"NSDictionary to JSONData error: %@", error);
        }
#endif
        return data;
#if __IPHONE_5_0 > __IPHONE_OS_VERSION_MIN_REQUIRED
    }else{
        ModuleLog(@"Using JSONKit to serialize JSON.");
        return [self JSONData];
    }
#endif
#else
    ModuleLog(@"Using JSONKit to serialize JSON.");
    return [self JSONData];
#endif
}

- (NSString *)universalConvertToJSONString{
#if OnlyUseJSONKit == 0
#if __IPHONE_5_0 > __IPHONE_OS_VERSION_MIN_REQUIRED
    if ([[[UIDevice currentDevice] systemVersion] intValue] >= 5) {
#endif
        ModuleLog(@"Using iOS5's NSJSONSerialization to serialize JSON.");
        NSError *error = nil;
        NSData *data = [NSJSONSerialization dataWithJSONObject:self
                                                       options:NSJSONWritingPrettyPrinted
                                                         error:&error];
        return [[[NSString alloc] initWithData:data
                                      encoding:NSUTF8StringEncoding] autorelease];
#if __IPHONE_5_0 > __IPHONE_OS_VERSION_MIN_REQUIRED
    }else{
        ModuleLog(@"Using JSONKit to serialize JSON.");
        return [self JSONString];
    }
#endif
#else
    ModuleLog(@"Using JSONKit to serialize JSON.");
    return [self JSONString];
#endif
}
@end
