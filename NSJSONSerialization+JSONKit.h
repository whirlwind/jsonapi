//
//  NSJSONSerialization+JSONKit.h
//  food
//
//  Created by 詹 迟晶 on 12-2-19.
//  Copyright (c) 2012年 BOOHEE. All rights reserved.
//
/** 这是一个用于JSON转换的通用接口
 *
 *  自动识别iOS版本，当iOS版本小于5时，调用JSONKit；当iOS版本大于等于5时，调用系统自带的NSJSONSerialization进行处理。
 *  当app需要部署到iOS4及以下时，需要引用JSONKit组件。
 */

////////////
#pragma mark Deserializing methods
////////////
@interface NSString (JSONDeserializing)
- (id)universalConvertToJSONObject;
- (id)universalConvertToJSONMutableObject;
@end

@interface NSData (JSONDeserializing)
// The NSData MUST be UTF8 encoded JSON.
- (id)universalConvertToJSONObject;
- (id)universalConvertToJSONMutableObject;
@end

////////////
#pragma mark Serializing methods
////////////

@interface NSString (JSONSerializing)
- (NSData *)universalConvertToJSONData;
- (NSString *)universalConvertToJSONString;
@end

@interface NSArray (JSONSerializing)
- (NSData *)universalConvertToJSONData;
- (NSString *)universalConvertToJSONString;
@end

@interface NSDictionary (JSONSerializing)
- (NSData *)universalConvertToJSONData;
- (NSString *)universalConvertToJSONString;
@end
