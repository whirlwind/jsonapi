//
//  NSObject+SBJSON.h
//  food
//
//  Created by 詹 迟晶 on 12-5-3.
//  Copyright (c) 2012年 BOOHEE. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSObject (SBJSON)
- (NSString *)JSONRepresentation;
@end
